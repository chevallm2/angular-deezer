import { Artist } from './artist';
import { Album } from './album';

export interface Track {
  title: string;

  preview: string;

  link: string;

  artist: Artist;

  album: Album;
}
