import { Directive, ElementRef, HostListener, Renderer2 } from '@angular/core';

@Directive({
  selector: '[appHoverable]'
})
export class HoverableDirective {

  private static readonly elevationClass = 'mat-elevation-z5';

  constructor(
    private el: ElementRef<HTMLElement>,
    private renderer: Renderer2
    ) {}

    @HostListener('mouseenter') onMouseEnter() {
      this.renderer.addClass(this.el.nativeElement, HoverableDirective.elevationClass);
    }

    @HostListener('mouseleave') onMouseLeave() {
      this.renderer.removeClass(this.el.nativeElement, HoverableDirective.elevationClass);
    }


  }
