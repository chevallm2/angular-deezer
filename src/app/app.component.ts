import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

import { DeezerService } from './deezer.service';
import { Track } from './track';
import { Observable, Subject } from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  form: FormGroup;
  tracks: Observable<Track[]>;

  previewTrack = '';

  @ViewChild('player', {static: true}) player: ElementRef;

  constructor(
    private fb: FormBuilder,
    private deezer: DeezerService
    ) {}

  ngOnInit() {
    this.initialiserFormulaire();
    this.initialiserAutocomplete();
    this.player.nativeElement.onloadeddata = (_: any) => this.player.nativeElement.play();
  }

  private initialiserFormulaire() {
    this.form = this.fb.group({
      recherche: ['', null]
    });
  }

  private initialiserAutocomplete() {
    this.form.get('recherche').valueChanges.subscribe( value => {
      if (value.length > 2) {
        this.search(value);
      }
    });
  }

  private search(query: string) {
    this.tracks = this.deezer.search(query);
  }

  playTrack(trackPreview: string) {
    this.previewTrack = trackPreview;
  }
}
