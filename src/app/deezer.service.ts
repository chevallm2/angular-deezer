import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';

import { Track } from './track';
import { environment } from 'src/environments/environment';

interface SearchOutput {
  data: Track[];

}

@Injectable({
  providedIn: 'root'
})
export class DeezerService {

  private readonly corsProxy = 'https://cors-anywhere.herokuapp.com/';
  private readonly url = 'https://api.deezer.com/search';

  constructor(private http: HttpClient) {}

  search(query: string): Observable<Track[]> {
    const params = new HttpParams()
      .set('output', 'json')
      .set('limit', '5')
      .set('q', query);
    const url = this.corsProxy + this.url;
    return this.http.get<SearchOutput>(url, {params}).pipe(
      map( output => output.data)
    );
  }
}
