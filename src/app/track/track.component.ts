import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Track } from '../track';

@Component({
  selector: 'app-track',
  templateUrl: './track.component.html',
  styleUrls: ['./track.component.css']
})
export class TrackComponent implements OnInit {

  @Input()
  track: Track;

  @Output()
  trackSelected = new EventEmitter<string>();
  constructor() { }

  ngOnInit() {
  }

  selectTrack(trackPreview: string) {
    this.trackSelected.emit(trackPreview);
  }

}
