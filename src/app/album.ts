export interface Album {

  cover_small: string;

  title: string;

}
